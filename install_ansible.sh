#! /bin/bash

echo '-------------------------'
echo 'Clear enviroment:        '
echo '-------------------------'
rm -Rf /home/user/android-emulator-container-scripts/
echo ''
echo '-------------------------'
echo 'Install Ansible:         '
echo '-------------------------'
apt update -y && apt install -y ansible
echo ''
echo '-------------------------'
echo 'Add host to ansible/host '
echo  '------------------------'
echo localhost > /etc/ansible/hosts && 
echo 'Done!'
echo ''
echo '-------------------------'
echo 'Run Ansible playbook     '
echo '-------------------------'
ansible-playbook -vv /home/user/playbook.yaml --connection=local
